﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management1
{
    public class mTask
    {
        public int TaskId { get; set; }
        public string Name { get; set; }
        //interms of task management
        public MachineResource AssignedTo { get; set; }
        //interms of responsibility 
        public Resoponsibility FPR { get; set; }
        //   public Post AssignedBy { get; set; }
        public bool AtStretch { get; set; }
    }

    //public class Machine
    //{
    //    public int MachineId { get; set; }
    //    public string Name { get; set; }

    //}

    //public class Person:BaseResource
    //{
    //    public Calender Calender { get; set; }
    //}

    public class PersonPost
    {
        public int PersonPostId { get; set; }
        public MachineResource Person { get; set; }
        public Post Post { get; set; }
        public DateTime Date { get; set; }
        //public string Designnation { get; set; }
    }
    public class Department
    {
        public int DepartmentId { get; set; }
        public string Name { get; set; }
        public ICollection<Department> Departments { get; set; }
        public ICollection<Post> Posts { get; set; } 
        public Calender Calender { get; set; }
    }
    public class Post
    {
        public int PostId { get; set; }
        public string Designation { get; set; }
        public virtual ICollection<Resoponsibility> Responsibilities { get; set; }
        public virtual ICollection<Post> Posts { get; set; } 
        public virtual Department Department { get; set; }
    }
    public class Resoponsibility
    {
        public int ResponsibilityId { get; set; }
        public string Name { get; set; }
    }

    public class ResourceInvolvementRequirement
    {
        public MachineResource Machine { get; set; }
        public double DegreeOfInvolvement { get; set; }
        public double workhour { get; set; }
        public virtual ICollection<ResourceAssignment> Assignments { get; set; }
    }

    public class ResourceAssignment
    {
        public int AssignmentId { get; set; }
        public virtual ICollection<ResourceInvolvementRequirement> InvolvementRequirementys { get; set; }
    }

    public class TaskResourceRequirement
    {
        public int TaskResourceRequirementId { get; set; }
        public mTask Task { get; set; }
        //serices of resource asignment
        public ICollection<ResourceAssignment> ResourceAssignments { get; set; }
    }

    public class MachineTask
    {
        public int MachineTaskId { get; set; }
        public MachineResource Machine { get; set; }
        public mTask Task { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public double DegreeOfInvolvement { get; set; }
    }
}
