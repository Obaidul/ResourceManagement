﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management1
{




    //what to achive
    public class Objective
    {
        public int ObjectiveId { get; set; }
        public string Name { get; set; }
        public BaseResource Resource { get; set; }
        public bool TargetValue { get; set; }
        public ICollection<ResourceCondition> Preconditions { get; set; }
    }

    public enum ComparisonType
    {
        Equal,
        GreaterEqual,
        LessEqual,
        Less,
        Greater,
        NotEqual,
    }

    public class ResourceCondition
    {
        public int ResourceConditionId { get; set; }
        public BaseResource Resource { get; set; }
        public object Value { get; set; }
        public ComparisonType Operator { get; set; }
    }

    //task to achive


    public enum ResourceOperationType
    {
        preservation,
        aquire,
        use
    }


    public class Unit
    {
        public int UnitId { get; set; }
        public string Name { get; set; }
        public double ConversionFactor { get; set; }
    }
    public class ResourceType
    {
        public int ResourceTypeId { get; set; }
        public string TypeName { get; set; }
        public Unit DefaultUnit { get; set; }
        public ICollection<Unit> Units { get; set; }
        public bool UseNeedPreAllocation { get; set; }
    }

    public class BaseResource
    {
        public int ResourceId { get; set; }
        public ResourceType resourceType { get; set; }
        public string Name { get; set; }
        public ICollection<Unit> OverridenUnits { get; set; }
        public Unit OverridenDefaultUnit { get; set; }
        public virtual Department Department { get; set; }
    }
    public class MaterialResource : BaseResource
    {
        public ResourcePack DefaultPack { get; set; }
    }
    public class MachineResource : BaseResource
    {
        public Calender Calender { get; set; }
    }

    public class PoolType
    {
        public int PoolTypeId { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Pool> Pools { get; set; }
    }

    public class ResourcePack
    {
        public int ResourcePackId { get; set; }
        public string Name { get; set; }
        public BaseResource Resource { get; set; }
        public double Quantity { get; set; }
        public ICollection<ResourceValue> Consumptions { get; set; }
    }

    public class Formula
    {
        public int FormulaId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public BaseResource Product { get; set; }
        public ICollection<ResourceValue> Compositions { get; set; }
    }
    public class Pool
    {
        public int PoolId { get; set; }
        public string Name { get; set; }
    }
    public class ResourcePool
    {
        public int ResourcePoolId { get; set; }
        public BaseResource Resource { get; set; }
        public Pool Pool { get; set; }

        public double Quantity { get; set; }
    }
    public class ResourceValue
    {
        public int ResourceValueId { get; set; }
        public BaseResource Resource { get; set; }
        public double Value { get; set; }
        public Unit Unit { get; set; }
    }

    public class ResourceTypeResoposibility
    {
        public int ResourceTypeResoposibilityId { get; set; }
        public ResourceType ResourceType { get; set; }
        public ResourceOperationType ResourceOperationType { get; set; }
        public Post Post { get; set; }
    }

    public class ResourceAllocationRequest
    {
        public int ResourceAllocationRequestID { get; set; }
        public Post RequestTo { get; set; }
        public BaseResource RequestFor { get; set; }
        public double QuantityRequestedForAllocation { get; set; }
    }

    public class ResourceAllocationRequestInbox
    {
        public int ResourceAllocationRequestID { get; set; }
        public Post Post { get; set; }

    }

    public class TimePoint
    {
        public int TimePointId { get; set; }
        public int Hr { get; set; }
        public int min { get; set; }
    }

    public class TimeSegment
    {
        public TimePoint StartTime { get; set; }
        public TimePoint EndTime { get; set; }
    }

    public class DayWorkingScheduleType
    {
        public int DayWorkingScheduleTypeId { get; set; }
        public string Name { get; set; }
        public TimeSegment WorkingTime { get; set; }
        public TimeSegment LunchBreak { get; set; }
        public TimeSegment EveningTime { get; set; }
    }
    public class BaseYearCalender
    {
        public int BaseCalanderId { get; set; }
        public virtual ICollection<DayCalender> WorkingDays { get; set; }
        public virtual ICollection<HolyDay> HolyDays { get; set; }
    }
    public class DayCalender
    {
        public int Day { get; set; }
        public DayWorkingScheduleType WorkingDays { get; set; }
    }
    public class HolyDay
    {
        public int HolydayId { get; set; }
        public int Day { get; set; }
        public string Name { get; set; }
    }
    public class Calender
    {
       // public int CalenderId { get; set; }
        public BaseYearCalender BaseCalender { get; set; }
        public virtual ICollection<DayCalender> OverrridenWorkingDays { get; set; }
        public virtual ICollection<HolyDay> OverrridenHolyDays { get; set; }
    }


    public class test
    {
        test()
        {
            //  var ResourcePool = new ResourcePool();

            var gm = new Unit { Name = "gm", ConversionFactor = .001 };
            var Kg = new Unit { Name = "Kg", ConversionFactor = 1 };
            var currency = new ResourceType
            {
                ResourceTypeId = 1,
                TypeName = "Currency",
                DefaultUnit = new Unit { UnitId = 1, Name = "DBT", ConversionFactor = 1 },
                UseNeedPreAllocation = true
            };
            var SolidMaterial = new ResourceType
            {
                ResourceTypeId = 1,
                TypeName = "Material",
                DefaultUnit = Kg,
                UseNeedPreAllocation = false,
                Units = new List<Unit> { gm }
            };
            var DirnkableLiquidFood = new ResourceType
            {
                ResourceTypeId = 3,
                TypeName = "Liquid Food",
                DefaultUnit = new Unit { Name = "Cup" },
                UseNeedPreAllocation = false
            };

            var taka = new MaterialResource { Name = "taka", ResourceId = 1, resourceType = currency };
            var sugar = new MaterialResource { Name = "Sugar", ResourceId = 2, resourceType = SolidMaterial };
            //sugar pack
            var sugar500gm = new ResourcePack
            {
                ResourcePackId = 1,
                Quantity = 0.5,
                Resource = sugar,
                Consumptions = new List<ResourceValue>
                     {
                         new ResourceValue { Resource=taka, Value=25 }
                     }
            };

            var sugar1kg = new ResourcePack
            {
                ResourcePackId = 2,
                Name = "1Kg pack",
                Quantity = 1,
                Resource = sugar,
                Consumptions = new List<ResourceValue>
                     {
                         new ResourceValue { Resource=taka, Value=46 }
                     }
            };
            sugar.DefaultPack = sugar1kg;




            var milkPowder = new MaterialResource { Name = "Milk Powder", ResourceId = 3, resourceType = SolidMaterial };
            //milk Pack
            var milk100gm = new ResourcePack { ResourcePackId = 3, Quantity = .1, Resource = milkPowder, Consumptions = new List<ResourceValue> { new ResourceValue { Resource = taka, Value = 43 } } };
            var milk250gm = new ResourcePack { ResourcePackId = 4, Quantity = .25, Resource = milkPowder, Consumptions = new List<ResourceValue> { new ResourceValue { Resource = taka, Value = 100 } } };
            var milk500gm = new ResourcePack { ResourcePackId = 5, Quantity = .50, Resource = milkPowder, Consumptions = new List<ResourceValue> { new ResourceValue { Resource = taka, Value = 195 } } };
            var milk1kgm = new ResourcePack { ResourcePackId = 6, Quantity = 1.0, Resource = milkPowder, Consumptions = new List<ResourceValue> { new ResourceValue { Resource = taka, Value = 362 } } };

            milkPowder.DefaultPack = milk500gm;

            var TeaLeaf = new BaseResource { Name = "Tea Leaf", ResourceId = 4, resourceType = SolidMaterial };
            //TeaLeaf Pack
            var TeaLeaf100gm = new ResourcePack { ResourcePackId = 7, Quantity = .1, Resource = TeaLeaf, Consumptions = new List<ResourceValue> { new ResourceValue { Resource = taka, Value = 18 } } };
            var TeaLeaf500gm = new ResourcePack { ResourcePackId = 7, Quantity = .5, Resource = TeaLeaf, Consumptions = new List<ResourceValue> { new ResourceValue { Resource = taka, Value = 82 } } };

            var Tea = new BaseResource { Name = "Tea", ResourceId = 5, resourceType = DirnkableLiquidFood };
            var TeaMakingFormula = new Formula
            {
                Code = "Tea01",
                Description = "General milk tea",
                FormulaId = 1,
                Product = Tea,
                Compositions = new List<ResourceValue>
                 {
                     new ResourceValue { Resource=TeaLeaf, Value=1, Unit=gm },
                     new ResourceValue {  Resource=sugar,Value=.5, Unit=Kg },
                     new ResourceValue {  Resource=milkPowder, Value=.002}
                 }
            };

            //Machine Resources

            var WorkHr = new Unit { UnitId = 7, Name = "Work Hour", ConversionFactor = 1 };


        }
    }
}
